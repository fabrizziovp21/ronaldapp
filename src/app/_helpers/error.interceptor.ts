import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                if(location.pathname=="/login"){
                    localStorage.setItem("Error","true");
                    localStorage.setItem("RefreshToken","false");
                }else{
                    localStorage.setItem("Path",location.pathname);
                    localStorage.setItem("RefreshToken","true");
                    location.replace("/login");
                }
            }
            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}