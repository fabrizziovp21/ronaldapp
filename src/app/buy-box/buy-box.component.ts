import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BoxBuildComponent } from '../box-build/box-build.component';

@Component({
  selector: 'app-buy-box',
  templateUrl: './buy-box.component.html',
  styleUrls: ['./buy-box.component.scss']
})
export class BuyBoxComponent implements OnInit {

  isLinear = false;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourdFormGroup: FormGroup;
  fifthFormGroup: FormGroup;

  @ViewChild(BoxBuildComponent) BoxBuildComponent: BoxBuildComponent;

  get frmStepOne() {
    return this.BoxBuildComponent ? this.BoxBuildComponent.frmStepOne : null;
  }

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
  }
}
