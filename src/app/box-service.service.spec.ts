import { TestBed, inject } from '@angular/core/testing';

import { BoxServiceService } from './box-service.service';

describe('BoxServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BoxServiceService]
    });
  });

  it('should be created', inject([BoxServiceService], (service: BoxServiceService) => {
    expect(service).toBeTruthy();
  }));
});
