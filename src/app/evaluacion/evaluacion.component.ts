import { Component, ViewChild, OnInit } from '@angular/core';
import {formatDate } from '@angular/common';
import {Router} from '@angular/router';
import { SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface} from 'ngx-swiper-wrapper';
import { AlertService, AuthenticationService } from '../_services';

export class Progreso
{
  preguntas: any;
  respuestas: any;
  progreso: any;
  idEvaluacion: any;
}
export class Respuestas
{
  texto: string;
  pregunta: Pregunta;
  evaluacion: Evaluacion;
}
export class Pregunta{
  idPregunta: Number;
}
export class Evaluacion{
  idEvaluacion: Number;
}
@Component({
  selector: 'app-evaluacion',
  templateUrl: './evaluacion.component.html',
  styleUrls: ['./evaluacion.component.scss']
})

export class EvaluacionComponent implements OnInit{
  current: number = 0;
  numero: number=0;
  data: Array<any>;
  public show: boolean = true;
  aumento: number;
  ans: Respuestas;
  respuestas: Array<Respuestas>=[];
  public opel(res, preg){
    let answer = new Respuestas();
    answer.texto=res;
    answer.pregunta=preg+1;
    if(this.ans.texto!=answer.texto){
      this.ans=answer;
      this.ver(this.ans);
    }else if (this.ans.texto==answer.texto && this.ans.pregunta!=answer.pregunta) {
      this.ans=answer;
      this.ver(this.ans);
    }
    console.log('index: '+ preg + ' respuesta: '+ res);
  }
  public ver(ans){
    if(this.respuestas[ans.pregunta-1]==undefined){
      this.respuestas[ans.pregunta-1]=new Respuestas();
      this.respuestas[ans.pregunta-1].pregunta=new Pregunta();
      this.respuestas[ans.pregunta-1].pregunta.idPregunta=this.data[String(this.elegido)].preguntas[ans.pregunta-1].idPregunta;
      this.respuestas[ans.pregunta-1].evaluacion=new Evaluacion();
      this.respuestas[ans.pregunta-1].evaluacion.idEvaluacion=this.data[String(this.elegido)].idEvaluacion;
    }
    if(this.data[0].preguntas[ans.pregunta-1].tipo.idTipoPregunta==1){
      if(this.respuestas[ans.pregunta-1].texto==undefined){
        this.respuestas[ans.pregunta-1].texto=ans.texto;  
        console.log(this.respuestas);
      }else{
        this.respuestas[ans.pregunta-1].texto=this.respuestas[ans.pregunta-1].texto+','+ans.texto;
        console.log(this.respuestas);
      }
    }else{
      this.respuestas[ans.pregunta-1].texto=ans.texto;
      console.log(this.respuestas);
    }
  }
  faltante:Number=0;
  elegido:any;
  user: Array<any>;
  progresion: Array<any>;
  respondidas: Array<any>;
  ngOnInit(){
    this.data=JSON.parse(localStorage.getItem("Evaluaciones"));
    console.log(this.data);
    this.user=JSON.parse(localStorage.getItem("User"));
    this.ans=new Respuestas();
    this.elegido=Number.parseInt(localStorage.getItem("evaluacionElegido"));
    this.actualizacion();
    this.respondidas=JSON.parse(localStorage.getItem("Respuesta"));
    this.progresion=JSON.parse(localStorage.getItem("Progresion"));
  }
  progreso: Array<Progreso>=[];
  public actualizacion(){
    this.authenticationService.respuesta()
      .subscribe((datares:any) => {
        localStorage.setItem("Respuesta",JSON.stringify(datares));
        for (let index = 0; index < this.data.length; index++) {
          this.progreso[index]=new Progreso();
          this.progreso[index].idEvaluacion=Number.parseInt(this.data[index].idEvaluacion);
          this.progreso[index].preguntas=Number.parseInt(this.data[index].preguntas.length);
          let h :any=0;
          for (let j = 0; j < datares.length; j++) {
              if (datares[j].evaluacion==this.progreso[index].idEvaluacion) {
                  h++;
                  this.progreso[index].respuestas=h-1;
              }
          }
          if (this.progreso[index].respuestas==NaN) {
            this.progreso[index].respuestas=h;
          }
          this.progreso[index].progreso=this.progreso[index].respuestas/this.progreso[index].preguntas*100;
        }
        localStorage.setItem("Progresion",JSON.stringify(this.progreso));
        },
  );
  }
  public verificaciondata(){
    for (let index = 0; index < this.respondidas.length; index++) {
      for (let c = 0; c < this.data[this.elegido].preguntas[this.respondidas[index].pregunta].alternativas.length; c++) {
        let no:any=0;
        if (this.data[this.elegido].preguntas[this.respondidas[index].pregunta].alternativas[c].alternativas==this.respondidas[index].texto){
          this.data[this.elegido].preguntas[this.respondidas[index].pregunta-1].alternativas[c].active= true;
        }
        if (no==0) {
          if(this.respondidas[index].pregunta==(this.respondidas[index+1].pregunta-1)){
            this.faltante=this.respondidas[index+1].pregunta-1;
          }  
        }
      }
    }
  }
  ngAfterContentInit(){
    this.verificaciondata();
  }
  anterior:Array<any>=[];
  public activateClass(opcion,i){
      if (this.anterior[i]==opcion) {
        this.anterior[i].active= !this.anterior[i].active;
        this.anterior[i]=opcion;
      }else if(this.anterior[i]==undefined){
        opcion.active= !opcion.active;
        this.anterior[i]=opcion;
      } else if(this.anterior[i]!=opcion){
        opcion.active= !opcion.active;
        this.anterior[i].active= !this.anterior[i].active;
        this.anterior[i]=opcion;
      }
  }
  public validacion(ans){
    let norepetido:boolean=true;
    for (let index = 0; index < this.respondidas.length; index++){
      if (this.respondidas[index].pregunta==ans.pregunta){
        norepetido=false;
      }
      if (this.respuestas[index].pregunta==ans.pregunta){
        norepetido=false;
      }
    }
    if (norepetido){
      console.log(norepetido);
      this.progresion[this.elegido].respuestas++;
      this.progresion[this.elegido].progreso=this.progresion[this.elegido].respuestas/this.progresion[this.elegido].preguntas*100;
      localStorage.setItem("Progresion",JSON.stringify(this.progresion));
    }
  }
  public type: string = 'component';
  public disabled: boolean = false;
  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'vertical',
    slidesPerView: 2,
    keyboard: false,
    mousewheel: true,
    scrollbar: false,
    navigation: true,
    pagination: false
  };
  private scrollbar: SwiperScrollbarInterface = {
    el: '.swiper-scrollbar',
    hide: false,
    draggable: true
  };
  private pagination: SwiperPaginationInterface = {
    el: '.swiper-pagination',
    clickable: true,
    hideOnClick: false
  };
  @ViewChild(SwiperComponent) componentRef?: SwiperComponent;
  @ViewChild(SwiperDirective) directiveRef?: SwiperDirective;
  name = 'Angular 6';
  today= new Date();
  jstoday = '';
  constructor( private authenticationService: AuthenticationService, private router: Router) {
    this.jstoday = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
  }
  public onIndexChange(index: number): void {
    console.log('Swiper index: ', index);
    console.log(this.ans.pregunta);
    if (this.ans.pregunta!=undefined) {
      this.enviar(Number(this.ans.pregunta)-1);
    }
  }
  public onSwiperEvent(event: string): void {
    console.log('Swiper event: ', event);
  }
  indexusados:Array<any>=[];
  public enviar(elegido){
    this.validacion(this.ans);
    if(this.indexusados[elegido]!=elegido){
      this.indexusados[elegido]=elegido;
      this.authenticationService.enviar(this.respuestas[elegido]).subscribe((data:any) => {
      },
    );
    }
  }
}
