import { Component, ViewChild, OnInit } from '@angular/core';
import {LoginComponent} from '../../login/login.component';
import { AlertService, AuthenticationService } from '../../_services';
export class Botones
{
  texto: string;
}
export class Progreso
{
  preguntas: any;
  respuestas: any;
  progreso: any;
  idEvaluacion: any;
}
@Component({
  selector: 'app-misevaluaciones',
  templateUrl: './misevaluaciones.component.html',
  styleUrls: ['./misevaluaciones.component.scss'],
  providers:[LoginComponent]
})

export class MisevaluacionesComponent implements OnInit {
  current: number = 0;
  items: Array<any>;
  user: Array<any>;
  respuesta: Array<any>;
  botones: Array<Botones>=[];
  expandedHeight: string="70px";
  collapsedHeight: string="70px";
  elegir(el){
    localStorage.setItem("evaluacionElegido",el);
  }
  progresion: Array<any>;
  constructor(private comp: LoginComponent, private authenticationService: AuthenticationService) {}
  ngOnInit(){
    console.log(localStorage.getItem("Error"));
    this.items=JSON.parse(localStorage.getItem("Evaluaciones"));
    this.user=JSON.parse(localStorage.getItem("User"));
    this.respuesta=JSON.parse(localStorage.getItem("Respuesta"));
    this.progresion=JSON.parse(localStorage.getItem("Progresion"));
  }
}
