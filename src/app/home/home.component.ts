import { Component, OnInit } from '@angular/core';
import {formatDate } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  name = 'Angular 6';
  today= new Date();
  jstoday = '';
  constructor() {
    this.jstoday = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');  
  }
  user:Array<any>;
  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem("User"));
  }
  
}
