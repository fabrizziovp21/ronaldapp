import { Component, OnInit } from '@angular/core';

export interface Pais {
  value: string;
  nombre: string;
}

@Component({
  selector: 'app-micuenta',
  templateUrl: './micuenta.component.html',
  styleUrls: ['./micuenta.component.scss']
})
export class MicuentaComponent {
  paises: Pais[] = [
    {value: 'p1', nombre: 'Peru'},
    {value: 'p2', nombre: 'Chile'}
  ];

}
