import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxBuildComponent } from './box-build.component';

describe('BoxBuildComponent', () => {
  let component: BoxBuildComponent;
  let fixture: ComponentFixture<BoxBuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxBuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxBuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
