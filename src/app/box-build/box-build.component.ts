import { Component, OnInit, Input, Attribute, Output, EventEmitter } from '@angular/core';
import { BoxServiceService } from '../box-service.service';
import { Paquete } from '../models/paquete';
import { Evaluacion } from '../models/evaluacion';
import { AttributeMarker } from '@angular/core/src/render3';
import { Router } from "@angular/router";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-box-build',
  templateUrl: './box-build.component.html',
  styleUrls: ['./box-build.component.scss']
})
export class BoxBuildComponent implements OnInit {
  [x: string]: any;

  message: number;
  maxEvaluations: number;
  selectedEvaluations: number = 0;

  disabled: boolean;

  constructor(private data: BoxServiceService, private formBuilder: FormBuilder) {
  }

  frmStepOne: FormGroup;

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message);
    if (this.message == null) {
      this.disabled = true;
      this.selectedEvaluations = 0;
    }
    this.frmStepOne = this.formBuilder.group({ secondCtrl2: [false, Validators.requiredTrue] });
  }

  private router: Router;
  evaluationNumber: number;

  sendIdBox(id: number) {
    this.disabled = false;
    this.data.changeMessage(id);
    this.maxEvaluations = this.paqueteArray.find(x => x.evaluationsNumber === this.message).evaluationsNumber;


    var element = document.getElementById("textoEvaluaciones");
    if (id == 1) {
      element.innerHTML = "Por favor, seleccione la evaluación que desee obtener:";
    } else {
      element.innerHTML = "Por favor, seleccione las " + this.maxEvaluations + " evaluaciones que desee obtener";
    }

    this.showPrice();

    if (this.maxEvaluations > 0 && this.maxEvaluations == this.selectedEvaluations) {
      this.frmStepOne = this.formBuilder.group({ secondCtrl2: [true, Validators.requiredTrue] });
      element.style.color = 'black'
    } else {
      if (this.selectedEvaluations > 0) {
        this.frmStepOne = this.formBuilder.group({ secondCtrl2: [false, Validators.requiredTrue] });
        element.style.color = 'red'
      }
    }

  }

  paqueteArray: Paquete[] = [
    {
      id: 1, title: "Box 1", subtitle: "01 evaluación + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando una de nuestras evaluaciones especializadas.",
      discount: " ", evaluationsNumber: 1, discountNumber: 0
    },
    {
      id: 2, title: "Box 2", subtitle: "02 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando dos de nuestras evaluaciones especializadas.",
      discount: "10% dscto", evaluationsNumber: 2, discountNumber: 10
    },
    {
      id: 3, title: "Box 3", subtitle: "03 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando tres de nuestras evaluaciones especializadas.",
      discount: "15% dscto", evaluationsNumber: 3, discountNumber: 15
    },
    {
      id: 4, title: "Box 4", subtitle: "04 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando tres de nuestras evaluaciones especializadas.",
      discount: "15% dscto", evaluationsNumber: 4, discountNumber: 15
    },
  ]

  evaluacionArray: Evaluacion[] = [
    { id: 1, name: "21 Competencias", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui offi", price: 1000 },
    { id: 2, name: "Inteligencia Emocional", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui offi", price: 1200 },
    { id: 3, name: "Competividad laboral", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui offi", price: 1500 },
  ]

  evaluationsId: string[] = [];

  checkEvaluationsNumber(checkbox: string) {

    if (this.findId(checkbox) != true) {
      this.evaluationsId.push(checkbox);
      this.selectedEvaluations++;
    } else {
      const index = this.evaluationsId.indexOf(checkbox);
      if (index > -1) {
        this.evaluationsId.splice(index, 1);
        this.selectedEvaluations--;
      }
    }

    this.showPrice();

    var element = document.getElementById("textoEvaluaciones");

    if (this.maxEvaluations > 0 && this.maxEvaluations == this.selectedEvaluations) {
      this.frmStepOne = this.formBuilder.group({ secondCtrl2: [true, Validators.requiredTrue] });
      element.style.color = 'black'
    } else {
      this.frmStepOne = this.formBuilder.group({ secondCtrl2: [false, Validators.requiredTrue] });
      element.style.color = 'red'
    }
  }

  findId(id: string) {
    var selectionItem = this.evaluationsId.find(function (element) {
      return element == id;
    });
    if (selectionItem === undefined) {
      return false;
    } else return true;
  }

  doPrice() {
    var price = 0;
    var j = this.evaluationsId.length;
    for (var i = 0; i < j; i++) {
      var idElement = +this.evaluationsId[i];
      price = price + this.evaluacionArray.find(x => x.id === idElement).price;
    }
    var dcto = 1 - (this.paqueteArray.find(x => x.id === this.message).discountNumber) / 100;
    return price * dcto;
  }

  showPrice() {
    this.doPrice();
    var precio = document.getElementById("price");
    precio.innerHTML = "Precio: S/  " + this.doPrice();
  }

  validateNext() {
    if (this.maxEvaluations > 0 && this.maxEvaluations == this.selectedEvaluations) {
      this.frmStepOne = this.formBuilder.group({ secondCtrl2: [true, Validators.requiredTrue] });
    } else {
      this.frmStepOne = this.formBuilder.group({ secondCtrl2: [false, Validators.requiredTrue] });
    }
  }

  cancel() {
    if (confirm("¿Está seguro que desea cancelar la compra?")) {
      this.router.navigate(['/index']);
      alert("holi")
    }
  }


}

