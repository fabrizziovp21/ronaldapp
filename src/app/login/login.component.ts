import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher, NativeDateAdapter} from '@angular/material/core';
import {Router} from '@angular/router';

import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AlertService, AuthenticationService } from '../_services';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
export class Progreso
{
  preguntas: any;
  respuestas: any;
  progreso: any;
  idEvaluacion: any;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  websiteRonald = "http://ronald.com.pe/";
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
    
  ]);
  isVisible: boolean=false;
  progreso: Array<Progreso>=[];
  items: Array<any>;
  loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
  matcher = new MyErrorStateMatcher();
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) { }
  ngOnInit(){
    localStorage.setItem("Error","false");
  }
  ngDoCheck(){
    if(localStorage.getItem("Error")=="false"){
      this.isVisible=false;
    }else{
      this.isVisible=true;
    }
  }
  loginUser(event){
    event.preventDefault();
    const target = event.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;
    localStorage.setItem("username",username);
    localStorage.setItem("password",password);
    this.authenticationService.login(username, password)
    .subscribe(data=> {
      this.authenticationService.user(username)
      .subscribe((res:any) => {
        localStorage.setItem("User",JSON.stringify(res));
        },
      );
      this.authenticationService.evaluacion()
      .subscribe((data:any) => {
        localStorage.setItem("Evaluaciones",JSON.stringify(data));
        },
      );
      this.authenticationService.respuesta()
      .subscribe((datares:any) => {
        this.items=JSON.parse(localStorage.getItem("Evaluaciones"));
        
        localStorage.setItem("Respuesta",JSON.stringify(datares));
        for (let index = 0; index < this.items.length; index++) {
          this.progreso[index]=new Progreso();
          this.progreso[index].idEvaluacion=Number.parseInt(this.items[index].idEvaluacion);
          this.progreso[index].preguntas=Number.parseInt(this.items[index].preguntas.length);
          let h :any=0;
          for (let j = 0; j < datares.length; j++) {
              if (datares[j].evaluacion==this.progreso[index].idEvaluacion) {
                  h++;
                  this.progreso[index].respuestas=h-1;
              }
          }
          if (this.progreso[index].respuestas==NaN) {
            this.progreso[index].respuestas=h;
          }
          this.progreso[index].progreso=this.progreso[index].respuestas/this.progreso[index].preguntas*100;
          console.log(this.progreso[index].progreso+' '+this.progreso[index].respuestas);
        }
        localStorage.setItem("Progresion",JSON.stringify(this.progreso));
        localStorage.setItem("Error","false");
        if (localStorage.getItem("RefreshToken")=="true") {
          localStorage.setItem("RefreshToken","false");
          location.replace(localStorage.getItem("Path"));
        }else{
          this.router.navigate(['login/home']);
        }
        },
      );
      },
    );
  }
}
