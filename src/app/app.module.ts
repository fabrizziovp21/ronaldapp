import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule,MatIconModule,MatListModule,MatSidenavModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
import { OwlModule } from 'ngx-owl-carousel';
import { DragScrollModule } from 'ngx-drag-scroll';
import {MatCardModule} from '@angular/material/card';
import { WavesModule, CardsFreeModule, MDBBootstrapModule  } from 'angular-bootstrap-md'
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes} from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IndexComponent } from './index/index.component';
import { HomeComponent } from './home/home.component';
import { MisevaluacionesComponent } from './home/misevaluaciones/misevaluaciones.component';
import { MicuentaComponent } from './home/micuenta/micuenta.component';
import { MatTabsModule} from '@angular/material/tabs';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { EvaluacionComponent } from './evaluacion/evaluacion.component';
import { SwiperModule, SwiperConfigInterface,
  SWIPER_CONFIG } from 'ngx-swiper-wrapper';
  import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';
import { BuyBoxComponent } from './buy-box/buy-box.component';
import {MatStepperModule} from '@angular/material/stepper';
import { BoxBuildComponent } from './box-build/box-build.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatDialogModule} from '@angular/material/dialog';
import { TerminadoComponent } from './terminado/terminado.component';
import { BoxRegisterComponent } from './box-register/box-register.component';
import { BoxValidationComponent } from './box-validation/box-validation.component';
import { BoxPaymentComponent } from './box-payment/box-payment.component';
import { BoxCulminationComponent } from './box-culmination/box-culmination.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { RestoreComponent } from './login/restore/restore.component';
import { FooterComponent } from './footer/footer.component';
import { FooterFixedComponent } from './footer-fixed/footer-fixed.component';

const routes: Routes =[
  {path: '', component: IndexComponent},
  {path: 'login', component: LoginComponent},
  {path: 'contacto', component: ContactUsComponent},
  {path: 'restaurarContrasena', component: RestoreComponent},
  {path: 'buyBox', component: BuyBoxComponent},
    {path: 'login/home', 
    component: HomeComponent,
    children:[
        {path:'',redirectTo:'misevaluaciones',pathMatch:'full'},
        {path:'misevaluaciones', component: MisevaluacionesComponent},
        {path:'micuenta', component:MicuentaComponent} 
    ]
    },
   {path: 'login/evaluacion', component: EvaluacionComponent},
   {path: 'login/terminado', component: TerminadoComponent},
   {path:'**', component:IndexComponent},    
];
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true
};
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    IndexComponent,
    HomeComponent,
    MisevaluacionesComponent,
    MicuentaComponent,
    EvaluacionComponent,
    BuyBoxComponent,
    BoxBuildComponent,
    TerminadoComponent,
    BoxRegisterComponent,
    BoxValidationComponent,
    BoxPaymentComponent,
    BoxCulminationComponent,
    ContactUsComponent,
    RestoreComponent,
    FooterComponent,
    FooterFixedComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    SwiperModule,
    MatButtonModule,
    MatRadioModule,
    CardsFreeModule,
    MatToolbarModule,
    MatStepperModule,
    WavesModule,
    MatTabsModule,
    MDBBootstrapModule.forRoot(),
    MatCardModule,
    MatExpansionModule,
    DragScrollModule,
    MatCheckboxModule,
    MatMenuModule,
    MatFormFieldModule,
    OwlModule,
    MatIconModule,
    MatListModule,
    MatCheckboxModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatTabsModule,
    MatExpansionModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSidenavModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    RouterModule.forRoot(routes,{useHash:true}),
    RouterModule.forRoot([
      {
        path: '',
        component: IndexComponent
      },
      {
        path: 'index',
        component: IndexComponent
      },
      {
        path: 'login/home',
        component: HomeComponent
      },
    ])
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    },
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
