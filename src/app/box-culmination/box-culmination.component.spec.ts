import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxCulminationComponent } from './box-culmination.component';

describe('BoxCulminationComponent', () => {
  let component: BoxCulminationComponent;
  let fixture: ComponentFixture<BoxCulminationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxCulminationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxCulminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
