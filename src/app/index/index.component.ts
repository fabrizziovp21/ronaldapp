import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Evaluacion } from 'src/app/models/evaluacion';
import { Paquete } from 'src/app/models/paquete';
import {Router, Scroll} from '@angular/router';
import { DragScrollComponent } from '../../../node_modules/ngx-drag-scroll/lib';
import { ScrollingVisibility, ScrollDispatcher } from '../../../node_modules/@angular/cdk/overlay';
import { BoxServiceService } from '../box-service.service';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})

export class IndexComponent implements OnInit {

  // idBox es el id del objeto Paquete a enviar a box-build al clickear "Adquirir"
  //@Input() idBox: number;

  panelOpenState = false;
  
  websiteRonald = "http://ronald.com.pe/";  
  carouselLink1 = "assets/imagenes/ronaldsld-2.jpg";
  carouselLink2 = "assets/imagenes/slideroutplacement.jpg";
  carouselLink3 = "assets/imagenes/sliderssessment.jpg";

  constructor(private router: Router, private data: BoxServiceService) {
  }

  evaluacionArray: Evaluacion[] = [
    {id: 1, name: "21 Competencias", detail: "Detalle de la evaluación 1", price: 1000},
    {id: 2, name: "Inteligencia Emocional", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui offi", price: 1200},
    {id: 3, name: "Competividad laboral", detail: "Detalle de la evaluación 3", price: 1500},
  ]
  
  paqueteArray: Paquete[] = [
    {
      id: 1, title: "Box 1", subtitle: "01 evaluación + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando una de nuestras evaluaciones especializadas.",
      discount: " ", evaluationsNumber: 1, discountNumber: 0
    },
    {
      id: 2, title: "Box 2", subtitle: "02 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando dos de nuestras evaluaciones especializadas.",
      discount: "10% dscto", evaluationsNumber: 2, discountNumber: 10
    },
    {
      id: 3, title: "Box 3", subtitle: "03 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando tres de nuestras evaluaciones especializadas.",
      discount: "15% dscto", evaluationsNumber: 3, discountNumber: 15
    },
    {
      id: 4, title: "Box 4", subtitle: "04 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando tres de nuestras evaluaciones especializadas.",
      discount: "15% dscto", evaluationsNumber: 4, discountNumber: 15
    },
  ]
  
  ngOnInit() {
  }  

  sendIdBox(id: number){
    this.data.changeMessage(id)
  }
}
