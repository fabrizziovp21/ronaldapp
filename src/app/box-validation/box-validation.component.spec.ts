import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxValidationComponent } from './box-validation.component';

describe('BoxValidationComponent', () => {
  let component: BoxValidationComponent;
  let fixture: ComponentFixture<BoxValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
