import { Component } from '@angular/core';
import { Evaluacion } from './models/evaluacion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'RonaldWebApp';
}
