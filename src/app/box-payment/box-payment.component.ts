import { Component, OnInit } from '@angular/core';
import { Paquete } from '../models/paquete';
import { Evaluacion } from '../models/evaluacion';
import { BoxServiceService } from '../box-service.service';

@Component({
  selector: 'app-box-payment',
  templateUrl: './box-payment.component.html',
  styleUrls: ['./box-payment.component.scss']
})
export class BoxPaymentComponent implements OnInit {

  constructor(private data: BoxServiceService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message);
  }

  //POR EL MOMENTO SE USARÁN ESTOS ARRAYS, LA IDEA ES QUE RECIBA LA DATA DESDE ANTES

  checked: true;
  disabled: true;
  message: number;

  paqueteArray: Paquete[] = [
    {
      id: 1, title: "Box 1", subtitle: "01 evaluación + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando una de nuestras evaluaciones especializadas.",
      discount: " ", evaluationsNumber: 1, discountNumber: 0
    },
    {
      id: 2, title: "Box 2", subtitle: "02 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando dos de nuestras evaluaciones especializadas.",
      discount: "10% dscto", evaluationsNumber: 2, discountNumber: 10
    },
    {
      id: 3, title: "Box 3", subtitle: "03 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando tres de nuestras evaluaciones especializadas.",
      discount: "15% dscto", evaluationsNumber: 3, discountNumber: 15
    },
    {
      id: 4, title: "Box 4", subtitle: "04 evaluaciones + asesoría",
      detail: "Descubre todo tu potencial evaluandote realizando tres de nuestras evaluaciones especializadas.",
      discount: "15% dscto", evaluationsNumber: 4, discountNumber: 15
    },
  ]

  evaluacionArray: Evaluacion[] = [
    { id: 1, name: "21 Competencias", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui offi", price: 1000 },
    { id: 2, name: "Inteligencia Emocional", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui offi", price: 1200 },
    { id: 3, name: "Competividad laboral", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui offi", price: 1500 },
  ]
}
