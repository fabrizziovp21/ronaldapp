import { Component, OnInit } from '@angular/core';
import {formatDate } from '@angular/common';

@Component({
  selector: 'app-terminado',
  templateUrl: './terminado.component.html',
  styleUrls: ['./terminado.component.scss']
})
export class TerminadoComponent implements OnInit {
  data: Array<any>;
  user: Array<any>;
  name = 'Angular 6';
  today= new Date();
  elegido: Number;
  jstoday = '';
  constructor() {
    this.jstoday = formatDate(this.today, 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');  
  }

  ngOnInit() {
    this.elegido=Number.parseInt(localStorage.getItem("evaluacionElegido"));
    this.data=JSON.parse(localStorage.getItem("Evaluaciones"));
    this.user=JSON.parse(localStorage.getItem("User"));
  }
  
}
