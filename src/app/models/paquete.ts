export class Paquete {
    id: number;
    title: string;
    subtitle: string;
    detail: string;
    discount: string;
    evaluationsNumber: number;
    discountNumber: number;
}