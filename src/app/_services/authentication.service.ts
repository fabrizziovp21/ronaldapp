import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

export class Arreglo{
    idEvaluacion: Number;
    nombre: String;
    instrucciones: String;
    descripcion:String;
    preguntas:subArreglo[]=[];
}
export class subArreglo{
    idPregunta: Number;
    enunciado: String;
    alternativas: Array<alternativasArray>=[];
    categoria: categoriasArray;
    tipo:tipoArray;
}
export class alternativasArray{
    alternativas:String;
}
export class categoriasArray{
    idCategoria:Number;
    nombre:String;
}
export class tipoArray{
    idTipoPregunta: Number;
    nombre: String;
}
export class Respuestas{
    idRespuesta:any;
    texto:String;
    pregunta:any;
    evaluacion:any;
}
@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }
    login(username: string, password: string):Observable<any>{
        return this.http.post<any>('http://alonsoleguia.tech:8080/appssessmentWS/login', 
        {username: username, password: password},{observe: 'response'}).pipe(map(data =>{
            localStorage.setItem("Token",data.headers.get('Authorization'));
            return data;
        }));
    }
    user(username:string){
        return this.http.get('http://alonsoleguia.tech:8080/appssessmentWS/users/'+username).pipe(map(res =>{
            return res;
        }));
    }
    enviar(body){
        return this.http.put('http://alonsoleguia.tech:8080/appssessmentWS/respuesta',body).pipe(map(res =>{
        }));
    }
    evaluacion(){
        return this.http.get('http://alonsoleguia.tech:8080/appssessmentWS/evaluacion').pipe(map(data =>{
            let arr: Arreglo[]=[];
            for (let index = 0; index<Object.keys(data).length; index++) {
                arr[index]=new Arreglo();
                arr[index].idEvaluacion=data[index].idEvaluacion;
                arr[index].nombre=data[index].nombre;
                arr[index].descripcion=data[index].descripcion;
                arr[index].instrucciones=data[index].instrucciones;
                for(let j=0;data[index].preguntas[j]!=null;j++){
                    arr[index].preguntas[j]=new subArreglo();
                    arr[index].preguntas[j].idPregunta=data[index].preguntas[j].idPregunta;
                    arr[index].preguntas[j].enunciado=data[index].preguntas[j].enunciado;
                    var split=data[index].preguntas[j].alternativas.split(',');
                    for (let m = 0; m < 5; m++) {
                        arr[index].preguntas[j].alternativas[m]= new alternativasArray();
                        arr[index].preguntas[j].alternativas[m].alternativas=split[m];
                    }
                    arr[index].preguntas[j].alternativas
                    arr[index].preguntas[j].categoria=new categoriasArray();
                    arr[index].preguntas[j].categoria.idCategoria=data[index].preguntas[j].categoria.idCategoria;
                    arr[index].preguntas[j].categoria.nombre=data[index].preguntas[j].categoria.nombre;
                    arr[index].preguntas[j].tipo=new tipoArray();
                    arr[index].preguntas[j].tipo.idTipoPregunta=data[index].preguntas[j].tipo.idTipoPregunta;
                    arr[index].preguntas[j].tipo.nombre=data[index].preguntas[j].tipo.nombre;
                }
                arr[index].preguntas.sort((a,b)=>(a.idPregunta>b.idPregunta) ? 1 : -1);
            }
            return arr;
        }));
    }
    respuesta(){
        return this.http.get('http://alonsoleguia.tech:8080/appssessmentWS/respuesta').pipe(map(resp =>{
            let respuesta: Respuestas[]=[];
            for (let index = 0; index<Object.keys(resp).length; index++) {
                respuesta[index]=new Respuestas();
                respuesta[index].idRespuesta=resp[index].idRespuesta;
                respuesta[index].texto=resp[index].texto;
                respuesta[index].pregunta=resp[index].pregunta;
                respuesta[index].evaluacion=resp[index].evaluacion;
            }
            for (let index = 0; index < respuesta.length; index++) {
                respuesta.sort((a,b)=>(a.pregunta>b.pregunta) ? 1 : -1);
            }
            return respuesta;
        }));
    }
    logout() {
        localStorage.removeItem("Token");
    }
}