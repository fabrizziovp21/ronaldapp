﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Token } from '../_models';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Token[]>('http://10.0.1.242:8080/login');
    }

    getById(id: number) {
        return this.http.get('http://10.0.1.242:8080/login' + id);
    }

    register(user: Token) {
        return this.http.post('http://10.0.1.242:8080/login', user);
    }

    update(user: Token) {
    }

    delete(id: number) {
        return this.http.delete('http://10.0.1.242:8080/login' + id);
    }
}